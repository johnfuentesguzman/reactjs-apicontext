import React, {useState, useContext} from 'react'
import GithubContext from '../../context/github/githubContext'; // getting context API
import AlertContext from '../../context/alert/alertContext'; // getting context API

const Search = () => {
    const githubContext = useContext(GithubContext); // esta constante tendrá TODO el contexto que incluyndo: reducer/state payload/ funciones) GithubState.js (value object) 
    const alertContext = useContext(AlertContext);
    const[text, setText] = useState('');

    const onSubmit = (e) => {
        e.preventDefault();
        if (text === '') {
            alertContext.setAlert('Plase enter Text to search', 'light', true)
            return;
        }

        githubContext.searchUsers(text); // searchUsers = GithubState.js (GithubContext.Provider - "value" object)

    }

    const onChange = (e) => {
        e.preventDefault();
        setText(e.target.value);
    }

    return (
        <div>
            <form className="form">
                <input type="text" name="text" placeholder="Search Users...." value={text} onChange={onChange} />
                <button className="btn btn-dark btn-block" type="button" onClick={onSubmit}>Search!</button>
            </form>
            {githubContext.users.length > 0 &&
                <button className="btn btn-light btn-block" onClick={githubContext.clearUsers}>Clear!</button>
            }

        </div>
    )

}

export default Search