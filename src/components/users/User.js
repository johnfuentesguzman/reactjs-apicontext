import React, { Fragment, useEffect, useContext } from 'react';
import Spinner from '../layout/Spinner';
import Repos from '../repos/Repos'
import { Link } from 'react-router-dom';
import GithubContext from '../../context/github/githubContext'; // getting context API

const User = ({ match }) => {
    const githubContext = useContext(GithubContext); // esta constante tendrá TODO el contexto que incluyndo: reducer/state payload/ funciones) GithubState.js (value object) 
    const {user, loading, repos} = githubContext;

    useEffect(() => {
        githubContext.getUser(match.params.login); // en app.js esta el parametro --> exact path='/user:login'
        githubContext.getUserRepos(match.params.login);
        // eslint-disable-next-line
    },[]); // IMPORTANT:  this square bracket "[]" allows us to stop a infinite loop when we try to fetch data, cause "useEffect" will be listening any update
    
    const { name,
        avatar_url,
        location,
        bio,
        blog,
        login,
        html_url,
        company,
        hireable,
        followers,
        following,
        public_repos,
        public_gists
    } = user;

    if (loading) return <Spinner />;
    return (
        <Fragment>
            <Link to='/' className='btn btn-light'>Back</Link>
            Hireable: {''}
            {hireable ?
                (<i className='fas fa-check - text-sucess' />)
                :
                (<i className='fas fa-times-circle - text-danger' />)
            }
            <div className='card -grid-2'>
                <div className='all-center'>
                    <img src={avatar_url} className='round-img' style={{ width: '10em' }} alt='' />
                    <div>{name}</div>
                    <p>Location: {location ? location : 'N/A'}</p>
                </div>
                <div>
                    {bio && (<Fragment>
                        <h3>Bio</h3>
                        <p>{bio}</p>
                    </Fragment>)
                    }
                    <a href={html_url} className='btn btn-dark my-1'>Visit GitHub Account</a>
                    <ul>
                        <li>
                            {login && (
                                <Fragment>
                                    <strong>Username: </strong>{login}
                                </Fragment>
                            )}
                        </li>
                        <li>
                            {company && (
                                <Fragment>
                                    <strong>Company: </strong>{company}
                                </Fragment>
                            )}
                        </li>
                        <li>
                            {blog && (
                                <Fragment>
                                    <strong>Website: </strong>{blog}
                                </Fragment>
                            )}
                        </li>
                    </ul>
                </div>
            </div>
            <div className='card text-center'>
                <div className='badge badge-primary'>Followers: {followers}</div>
                <div className='badge badge-success'>Following: {following}</div>
                <div className='badge badge-light'>Public Repost: {public_repos}</div>
                <div className='badge badge-dark'>Public Gist: {public_gists}</div>
            </div>
            <Repos repos={repos} />
        </Fragment>
    )

}

export default User
