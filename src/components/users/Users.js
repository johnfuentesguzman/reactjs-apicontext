import React, {useContext} from 'react';
import UserItem from './UserItem';
import Spinner from '../layout/Spinner';
import PropTypes from 'prop-types'// type catching
import GithubContext from '../../context/github/githubContext'; // getting context API

const Users = () => {
    const githubContext = useContext(GithubContext); // esta constante tendrá TODO el contexto que incluyendo: reducer/state payload/ funciones) GithubState.js (value object)
    const {users, loading} = githubContext;

    if (loading){
        return <Spinner/>;
    }

    return (
        <div style={userComponentStyle} >
            {users.map(user => (
                <UserItem key={user.id} user={user} />
            ))}
        </div>
    )
}

UserItem.propTypes ={
    users:  PropTypes.array.isRequired,
    loading:  PropTypes.bool.isRequired
}

const userComponentStyle = {
    display: 'grid',
    gridTemplateColumns: 'repeat (3, 1fr)',
    gridGap: '1rem'
}

export default Users;

