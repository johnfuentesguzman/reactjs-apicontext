import React from 'react'

const NotFound = () => (
    <div>
        <h1>Not Found</h1>
        <p className="lead">Oops! Page you are looking for, Not Exist.</p>
    </div>
)


export default NotFound
