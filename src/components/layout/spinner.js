import React, { Fragment } from 'react'
import SpinnerImage from './spinner.gif'

const Spinner = () => {
    return (
        <div>
            <Fragment>
                <img src={SpinnerImage}  alt= 'Loading...' style={{width: '200px', margin: 'auto', display:'block' }} />
            </Fragment>
            
        </div>
    )
}

export default Spinner;
