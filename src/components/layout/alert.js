import React, { useContext } from  'react'
import AlertContext from '../../context/alert/alertContext'; // getting context API

const Alert = () => {
    const alertContext = useContext(AlertContext); // esta constante tendrá TODO el contexto que incluyndo: reducer/state payload/ funciones) GithubState.js (value object) 
    const {alert} = alertContext;
    return (
        alert != null  &&  (
            <div className={`alert alert-${alert.type}`}>
                <i className = 'fas fa-info-circle'></i> {alert.msg}
                
            </div>
        )
    )
}

export default Alert;
