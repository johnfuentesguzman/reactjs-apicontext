import React, { useReducer } from 'react';
import AlertContext from './alertContext'
import AlertReducer from './alertReducer'
import {
    SET_ALERT,
    REMOVE_ALERT

} from '../types';


const AlertState = props => {
    const initialState = null; //Estado inicial, no es necesario en este caso crear un objeto/payload

    const [state, dispatch] = useReducer(AlertReducer, initialState);

    // Set alert
    const setAlert = (msg, type) => {
      dispatch({type: SET_ALERT, payload:{msg, type}})
      setTimeout(() => dispatch({ type:REMOVE_ALERT }), 8000);
  
    }
    return <AlertContext.Provider
        // value =  objeto que define todos los datos/funciones que los componentes hijos pueden acceder
        value={{
            alert: state,
            setAlert, // funcion
        }}
    >
        {props.children}
    </AlertContext.Provider>
};

export default AlertState;