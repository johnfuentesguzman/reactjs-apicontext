import {
    SET_ALERT,
    REMOVE_ALERT,

} from '../types';

// state = what is the current data, 
// action = all parameters send by dispatch, for example:  dispatch({type: GET_USERS, payload: res.data });
export default (state, action) => {
    switch (action.type) {
        case SET_ALERT:
            return action.payload;            
        case REMOVE_ALERT:
            return null;
        default:
            return state;
    }
};