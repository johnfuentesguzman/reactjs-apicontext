import React, { useReducer } from 'react';
import axios from 'axios';
import GithubContext from './githubContext'
import GithubReducer from './githubReducer'
import {
    SEARCH_USERS,
    SET_LOADING,
    CLEAR_USERS,
    GET_USER,
    GET_REPOS

} from '../types';

let gitHubClientId;
let gitHubClientSecret;

if( process.env.NODE_ENV !== 'production'){
    gitHubClientId = process.env.REACT_APP_GITHUB_ID;
    gitHubClientSecret = process.env.REACT_APP_GITHUB_CLIENT_SECRET;
}else{
    gitHubClientId = process.env.GITHUB_CLIENT_ID;
    gitHubClientSecret = process.env.GITHUB_CLIENT_SECRET;
}

const GithubState = props => {
    const initialState = {
        users: [],
        user: {},
        repos: [],
        loading: false
    }

    const [state, dispatch] = useReducer(GithubReducer, initialState);

    // Search users
    const searchUsers = async user => {
        setLoading(); //It will CALL the const/function "const setLoading = () =>  dispatch({type: SET_LOADING })"

        const res = await axios.get(`https://api.github.com/search/users?q=${user}
        &client_id=${gitHubClientId}
        &client_secret=${gitHubClientSecret}`);

        dispatch({ type: SEARCH_USERS, payload: res.data.items });  // parameters = the action name , the data payload to set
    }

    
    // Get singles user details
    const getUser = async (username) => {
        setLoading();
        const res = await axios.get(`https://api.github.com/users/${username}
                    ?client_id=${gitHubClientId}
                    &client_secret=${gitHubClientSecret}`);
        dispatch({ type: GET_USER, payload: res.data });  // parameters = the action name , the data payload to set
    };

    // get user git repos info
    const getUserRepos = async (username) => {
        setLoading();
        const res = await axios.get(`https://api.github.com/users/${username}/repos?per_page=5&sort=created:asc
                    &client_id=${gitHubClientId}
                    &client_secret=${gitHubClientSecret}`);
        dispatch({ type: GET_REPOS, payload: res.data });
    };

    // Clear Users
    const clearUsers = () => dispatch({ type: CLEAR_USERS })

    // Set Loading
    const setLoading = () => dispatch({ type: SET_LOADING })

    return <GithubContext.Provider
        // value =  objeto que define todos los datos/funciones que los componentes hijos pueden acceder
        value={{
            users: state.users,
            user: state.user,
            repos: state.repos,
            loading: state.loading,
            searchUsers, // Es una funcion = Se le permite a todos los componentes del app usarla
            clearUsers, // funcion
            getUser, // funcion
            getUserRepos // funcion
        }}
    >
        {props.children}
    </GithubContext.Provider>
};

export default GithubState;