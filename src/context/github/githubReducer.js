import {
    SEARCH_USERS,
    SET_LOADING,
    CLEAR_USERS,
    GET_USER,
    GET_REPOS

} from '../types';

// state = what is the current data, 
// action = all parameters send by dispatch, for example:  dispatch({type: GET_USERS, payload: res.data });
export default (state, action) => {
    switch (action.type) {
        case SEARCH_USERS:
            return {
                ...state, // ... = Copiara el payload tal como este , es ese momento
                users: action.payload,
                loading: false
            }            
        case GET_USER:
            return {
                ...state, // ... = Copiara el payload tal como este , es ese momento
                user: action.payload,
                loading: false
            }

        case GET_REPOS:
            return {
                ...state, // ... = Copiara el payload tal como este , es ese momento
                repos: action.payload,
                loading: false
            }
        case CLEAR_USERS:
            return {
                ...state,
                users: [],
                loading: false
            }

        case SET_LOADING:
            return {
                ...state,
                loading: true
            }

        default:
            return state;
    }
};